"""
 This file is part of the GetWVKeys project (https://github.com/GetWVKeys/getwvkeys)
 Copyright (C) 2022 Notaghost, Puyodead1 and GetWVKeys contributors 
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published
 by the Free Software Foundation, version 3 of the License.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# modified from getwvkeys.py to work with udemy by puyodead1

import argparse
import base64
import json
import requests
import sys
import xmltodict

version = "5.1"
API_URL = "__getwvkeys_api_url__"

# Change your headers here
def headers():
    return {
        'authority': 'www.udemy.com',
        'pragma': 'no-cache',
        'cache-control': 'no-cache',
        'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="97", "Chromium";v="97"',
        'accept': 'application/json, text/plain, */*',
        'dnt': '1',
        'content-type': 'application/octet-stream',
        'sec-ch-ua-mobile': '?0',
        # seems to cause cloudflare captchas to trigger
        # 'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36',
        'sec-ch-ua-platform': '"Windows"',
        'origin': 'https://www.udemy.com',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'accept-language': 'en-US,en;q=0.9',
    }


# CHANGE THIS FUNCTION TO PARSE LICENSE URL RESPONSE
def post_request(arg, challenge):
    print(arg)
    r = requests.post(arg.url, headers=arg.headers, data=challenge, timeout=10)
    if arg.verbose:
        # printing the raw license data can break terminals
        print("[+] License response:\n", base64.b64encode(r.content).decode("utf-8"))
    if not r.ok:
        print("[-] Failed to get license: [{}] {}".format(r.status_code, r.text))
        exit(1)
    return r.content


# Do Not Change Anything in this class
class GetwvCloneApi:
    def __init__(self, arg) -> None:
        # dynamic injection of the API url
        self.baseurl = "https://getwvkeys.cc" if API_URL == "__getwvkeys_api_url__" else API_URL
        self.api_url = self.baseurl + "/pywidevine"
        self.args = arg
        self.args.headers = headers()
        self.args.pssh = self.get_pssh()
        if self.args.verbose:
            print(f"PSSH: {self.args.pssh}")

    def read_pssh_from_bytes(self, bytes):
        pssh_offset = bytes.rfind(b'pssh')
        _start = pssh_offset - 4
        _end = pssh_offset - 4 + bytes[pssh_offset-1]
        pssh = bytes[_start:_end]
        return pssh

    def get_init_url(self):
        if self.args.verbose:
            print("Fetching manifest")
        res = requests.get(self.args.mpd_url)
        res.raise_for_status()
        xml = res.content
        mpd = xmltodict.parse(xml)
        sets = mpd["MPD"]["Period"]["AdaptationSet"]
        video_set = sets[0]
        video_rep = video_set["Representation"][-1]
        init_url = video_rep["SegmentTemplate"]["@initialization"]
        return init_url

    def get_pssh(self):
        if self.args.verbose:
            print("Extracting PSSH")
        init_url = self.get_init_url()
        if init_url is None:
            raise Exception("Failed to extract init url")
        if self.args.verbose:
            print(f"Init URL: {init_url}")
        res = requests.get(init_url, headers=self.args.headers)
        if not res.ok:
            raise Exception("Could not download init segment: " + res.text)
        pssh = self.read_pssh_from_bytes(res.content)
        return base64.b64encode(pssh).decode("utf8")

    def generate_request(self):
        if self.args.verbose:
            print("[+] Generating License Request ")
        data = {"pssh": self.args.pssh, "buildInfo": self.args.buildinfo, "force": self.args.force, "license_url": self.args.url}
        header = {"X-API-Key": self.args.api_key, "Content-Type": "application/json"}
        r = requests.post(self.api_url, json=data, headers=header)
        if not r.ok:
            if "error" in r.text:
                # parse the response as a json error
                error = json.loads(r.text)
                print("[-] Failed to generate license request: [{}] {}".format(error.get("code"), error.get("message")))
                exit(1)
            print("[-] Failed to generate license request: [{}] {}".format(r.status_code, r.text))
            exit(1)

        data = r.json()

        if "X-Cache" in r.headers:
            if args.verbose:
                print(json.dumps(data, indent=4))
            print("\n" * 5)
            print("[+] Keys (Cache):")
            for k in data["keys"]:
                print("--key {}".format(k["key"]))
            exit(0)

        self.session_id = data["session_id"]
        challenge = data["challenge"]

        if self.args.verbose:
            print("[+] License Request Generated\n", challenge)
            print("[+] Session ID:", self.session_id)

        return base64.b64decode(challenge)

    def decrypter(self, license_response):
        if self.args.verbose:
            print("[+] Decrypting with License Request and Response ")
        data = {
            "pssh": self.args.pssh,
            "response": license_response,
            "license_url": self.args.url,
            "headers": self.args.headers,
            "buildInfo": self.args.buildinfo,
            "force": self.args.force,
            "session_id": self.session_id,
        }
        header = {"X-API-Key": self.args.api_key, "Content-Type": "application/json"}
        r = requests.post(self.api_url, json=data, headers=header)
        if not r.ok:
            if "error" in r.text:
                # parse the response as a json error
                error = json.loads(r.text)
                print("[-] Failed to decrypt license: [{}] {}".format(error.get("code"), error.get("message")))
                exit(1)
            print("[-] Failed to decrypt license: [{}] {}".format(r.status_code, r.text))
            exit(1)
        return r.json()

    def main(self):
        license_request = self.generate_request()
        if args.verbose:
            print(f"Sending License URL Request")
        license_response = post_request(args, license_request)
        decrypt_response = self.decrypter(base64.b64encode(license_response).decode())
        keys = decrypt_response["keys"]
        session_id = decrypt_response["session_id"]

        if args.verbose:
            print(json.dumps(decrypt_response, indent=4))
            print("Decryption Session ID:", session_id)
        print("\n" * 5)
        print("[+] Keys:")
        for k in keys:
            print("--key {}".format(k))


if __name__ == "__main__":
    getwvkeys_api_key = "__getwvkeys_api_key__"
    print(f"\n{' ' * 6}pywidevine-api {version}\n{' ' * 7} from getwvkeys \n\n")

    parser = argparse.ArgumentParser()
    parser.add_argument('-media_license_token', help='Media License Token')
    parser.add_argument('-mpd_url', help='MPD Dash Manifest URL')
    parser.add_argument('-api_key', help='GetWVKeys API Key')
    parser.add_argument("--force", "-f", help="Force fetch, bypasses cache (You should only use this if the cached keys are not working). Default is OFF", default=False, action="store_true")
    parser.add_argument('--verbose', "-v",
                        help="increase output verbosity", action="store_true")
    parser.add_argument('--buildinfo', '-b', default="",
                        help='Buildinfo', required=False)

    args = parser.parse_args()
    args.api_key = getwvkeys_api_key if getwvkeys_api_key != "__getwvkeys_api_key__" else args.api_key

    if len(sys.argv) == 1:
        parser.print_help()
        print()
        while (args.media_license_token is None and args.mpd_url is None) or (args.media_license_token == "" and args.mpd_url == "" and args.api_key == ""):
            args.media_license_token = input('Enter Media License Token: ')
            args.mpd_url = input('Enter MPD URL: ')
            args.api_key = input('GetWVKeys API Key: ')
        args.buildinfo = ""
        args.verbose = False
    if not args.force:
        args.force = False
    args.url = f"https://www.udemy.com/api-2.0/media-license-server/validate-auth-token?drm_type=widevine&auth_token={args.media_license_token}"

    try:
        start = GetwvCloneApi(args)
        start.main()
    except Exception as e:
        raise

    print("\nDONE\n")
